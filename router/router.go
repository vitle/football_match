package router

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	user "gitlab.com/vitle/football_user/core"
	"gitlab.com/vitle/football_user/database"
)

func RegisterRouter(r *gin.Engine, uMgr *user.UserMgr) {
	r.GET("/ping", Ping)
	r.POST("/user", RegisterUser)
	r.POST("/user/:id", UpdateUser)
	r.GET("/user/:id", GetUser)
	r.GET("/users", ListUser)
	r.DELETE("/user/:id", DeleteUser)

	r.POST("/login", Login)
	r.GET("/logout", Logout)
	r.GET("/reset_token", ResetToken)
}

func ResetToken(c *gin.Context) {

}

func Logout(c *gin.Context) {

}

func Login(c *gin.Context) {

}

func DeleteUser(c *gin.Context) {

}

func ListUser(c *gin.Context) {

}

func GetUser(c *gin.Context) {

}

type UserUpdate struct {
	Id       string `binding:"required"`
	Username string `binding:"required"`
	Address  string
	Avatar   string
}

func UpdateUser(c *gin.Context) {
	var json UserUpdate
	if err := c.ShouldBind(&json); err != nil {
		fmt.Println("err")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

}

type UserCreated struct {
	Username string `binding:"required"`
	Email    string `binding:"required"`
	Address  string
	Avatar   string
}

func RegisterUser(c *gin.Context) {
	// username := c.Param("username")
	// email := c.Param("email")
	var json UserCreated
	if err := c.ShouldBindJSON(&json); err != nil {
		fmt.Println("loi o day")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	utmp := &database.User{Email: json.Email}

	fu := uMgr.FindUser(utmp)
	if fu.RowsAffected != 0 {
		c.JSON(http.StatusBadRequest, gin.H{"message": "email exists"})
		return
	}

	user := &database.User{
		Id:       uuid.NewV4().String(),
		Username: json.Username,
		Email:    json.Email,
	}
	result := uMgr.CreateUser(user)
	if result.Error != nil {
		c.JSON(500, gin.H{
			"message": result.Error,
		})
	}
	c.JSON(200, gin.H{
		"data": result.Value,
	})
}

func Ping(c *gin.Context) {
	c.String(200, "pong")
}
