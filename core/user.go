package user

import (
	"database/sql"

	"github.com/jinzhu/gorm"
	"gitlab.com/vitle/football_user/database"
)

type UserMgr struct {
	db  *sql.DB
	dbo *gorm.DB
}

func InitUserMgr(uMgr *UserMgr) {
	db, err := database.ConnectDb()
	if err != nil {
		panic(err)
	}
	dbOrm := database.ConnectDBORM()

	uMgr.db = db
	uMgr.dbo = dbOrm
}

func CreateAccesstoken() string {
	return ""
}

func (uMgr *UserMgr) CreateUser(user *database.User) *gorm.DB {
	return uMgr.dbo.Create(user)
}

func (uMgr *UserMgr) UpdateUser(user *database.User, id string) *gorm.DB {
	u := &database.User{}
	uMgr.dbo.First(u, id)
	return uMgr.dbo.Model(&u).Updates(user)
}

func (uMgr *UserMgr) GetUser(id string) *gorm.DB {
	u := &database.User{}
	return uMgr.dbo.First(u, id)
}

func (uMgr *UserMgr) FindUser(user *database.User) *gorm.DB {
	u := &database.User{}
	return uMgr.dbo.Where(user).First(u)
}

func (uMgr *UserMgr) ListUser() *gorm.DB {
	users := []*database.User{}
	return uMgr.dbo.Find(users)
}

func (uMgr *UserMgr) Delete(id string) *gorm.DB {
	user := &database.User{Id: id}
	return uMgr.dbo.Delete(user)
}
