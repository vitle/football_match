package main

import (
	"fmt"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/urfave/cli"
	user "gitlab.com/vitle/fb_user/core"
	"gitlab.com/vitle/football_user/database"
	"gitlab.com/vitle/football_user/router"
)

var r *gin.Engine
var uMgr *user.UserMgr

func main() {
	app := cli.NewApp()
	app.Name = "User api"
	app.Usage = "Manage user api"

	app.Action = func(c *cli.Context) error {
		fmt.Println("cannot found")
		return nil
	}

	app.Commands = []cli.Command{{
		Name:   "createdb",
		Usage:  "create match database",
		Action: database.Createdb,
	}, {
		Name:   "createdborm",
		Usage:  "create match database",
		Action: database.Createdborm,
	}, {
		Name:   "start",
		Usage:  "start service match",
		Action: start,
	}}

	if err := app.Run(os.Args); err != nil {
		panic(err)
	}

}

func initService() {
	uMgr = &user.UserMgr{}
	user.InitUserMgr(uMgr)
}

func start(c *cli.Context) {

	initService()
	r = gin.Default()
	router.RegisterRouter(r, uMgr)
	r.Run(":80")
}
