package database

type User struct {
	Id       string `gorm:"primary_key"`
	Username string
	Email    string
	Address  string
	Avatar   string
	IsBan    bool
	Coin     int64
	IsAdmin  string `gorm:"default:'0'"`
}

type Token struct {
	Id         string
	UserId     string
	Token      string
	ResetToken string
}

type Walletlog struct {
	Id            string `gorm:"primary_key"`
	UserId        string
	Balance       float64
	Currency      string
	Coin          int64
	TransactionId string
	Created       int64
}

type Gamelog struct {
	Id      string `gorm:"primary_key"`
	UserId  string
	Coin    int64
	Token   string
	Created int64
}
