package database

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func ConnectDb() (*sql.DB, error) {
	fmt.Println("connect db")
	db, err := sql.Open("mysql", "root:secret@:3307/")
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return db, nil
}

func ConnectDBORM() *gorm.DB {
	db, err := gorm.Open("mysql", "root:secret@:3307/user?charset=utf8&parseTime=True&loc=Local")
	// defer db.Close()
	if err != nil {
		panic(err)
	}
	return db
}
