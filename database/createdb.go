package database

import (
	"fmt"

	"github.com/urfave/cli"
)

func Createdborm(c *cli.Context) {
	fmt.Println("create db orm")
	db := ConnectDBORM()
	db.AutoMigrate(&User{}, &Walletlog{}, &Gamelog{})

}

func Createdb(c *cli.Context) {
	fmt.Println("create db")
	db, err := ConnectDb()
	if err != nil {
		panic(err)
	}
	_, err = db.Exec(`CREATE DATABASE IF NOT EXISTS match CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;`)
	if err != nil {
		panic(err)
	}

	_, err = db.Exec(`USE match`)
	if err != nil {
		panic(err)
	}

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS matchs (
		id VARCHAR(255) PRIMARY KEY,
		name VARCHAR(255),
		tournament_id VARCHAR(255),
		link_vip VARCHAR(255),
		link_free VARCHAR(255),
		time_start TIMESTAMP,
		created_at TIMESTAMP,
		updated_at TIMESTAMP
		)`)

	if err != nil {
		fmt.Println("createdb failed -", err)
		panic(err)
	}
	fmt.Println("created table match")

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS team_matchs (
		id VARCHAR(255) PRIMARY KEY,
		team_id VARCHAR(255),
		goal INT,
		yellow INT,
		red INT,
		match_id VARCHAr(255),
		updated_at TIMESTAMP,
		created_at TIMESTAMP
	)`)

	if err != nil {
		panic(err)
	}

	fmt.Println("created table team_match")

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS teams (
		id VARCHAR(255) PRIMARY KEY,
		name VARCHAR(255),
		description text,
		updated_at TIMESTAMP,
		created_at TIMESTAMP
	)`)

	if err != nil {
		panic(err)
	}

	fmt.Println("created table teams")

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS tournaments (
		id VARCHAR(255) PRIMARY KEY,
		name VARCHAR(255),
		time_start TIMESTAMP,
		time_end TIMESTAMP, 
		description text,
		updated_at TIMESTAMP,
		created_at TIMESTAMP
	)`)

	if err != nil {
		panic(err)
	}

	fmt.Println("created table teams")
}
